FizzBuzz
================================================
Coding Kata: [FizzBuzz on Coding Dojo](https://codingdojo.org/kata/FizzBuzz/)

Qt window is complete for exercise. Task is to implement the above kata within the FizzBuzz class.

Try to use Clean Code principles, especially
- [Function Rules](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29#functions-rules)
- [Comment Rules](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29#comments-rules)

Qt Interface
================================================
See _FizzBuzz/Resources/FizzBuzzWithQt.mp4_ for a video demonstration.

![](Resources/MainWindow.png)